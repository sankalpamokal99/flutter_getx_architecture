import 'package:flutter/material.dart';

class Style {
  static Widget getText({String data}) {
    return Text(
      "$data",
      style: TextStyle(fontSize: 20, color: Colors.white),
    );
  }

  static Widget getTitleText({String data}) {
    return Text(
      "$data",
      style: TextStyle(fontSize: 30),
    );
  }
}
