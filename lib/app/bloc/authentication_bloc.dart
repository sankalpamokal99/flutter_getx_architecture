import 'package:flutter_getx_mvc/app/data/repository/main_repository.dart';

class AuthenticationBloc {
  final MainRepository _repository = MainRepository();

  Future<dynamic> authenticate(Map params) {
    return _repository.authenticate(params);
  }

  @override
  void dispose() {}
}
