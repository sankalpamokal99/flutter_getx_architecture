import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_getx_mvc/app/data/provider/custom_exception.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Utils {
  static bool isMobile(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    var isMobile = shortestSide < 600;

    if (isMobile) {
      return true;
    } else {
      return false;
    }
  }

  static void back(BuildContext context) {
    if (Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
    }
  }

  static void setStatusBarColor(Color color) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: color,
    ));
  }

  static Future<bool> isConnectedToInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }

  static Future<bool> checkInternetAndShowMsg() async {
    bool result = await isConnectedToInternet();
    if (!result) {
      Fluttertoast.showToast(msg: CustomException.ERROR_NO_INTERNET_CONNECTION);
    }
    return result;
  }

  static void showLoadingDialog(BuildContext context) {
    showGeneralDialog(
        context: context,
        barrierColor: Colors.white.withOpacity(0.2),
        barrierDismissible: false,
        transitionDuration: Duration(milliseconds: 0),
        pageBuilder: (context, animation, secondaryAnimation) =>
            SizedBox.expand(
              // makes widget fullscreen
              child: Center(
                child: Card(
                    color: Colors.white.withOpacity(.9),
                    elevation: 4,
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: new CircularProgressIndicator(
                        strokeWidth: 2,
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.grey),
                      ),
                    )),
              ),
            ));
  }

  static void dismissLoadingDialog(BuildContext context) {
    if (Navigator.canPop(context)) {
      Navigator.of(context).pop();
    }
  }

  static void showErrorSnackBar(String error) {
    // Get.snackbar("Error", error,
    //     snackPosition: SnackPosition.BOTTOM,
    //     backgroundColor: Colors.redAccent,
    //     margin: EdgeInsetsResponsive.only(bottom: 10, left: 10, right: 10));
  }

  static void showSucessSnackBar(String error) {
    // Get.snackbar("Sucess", error,
    //     snackPosition: SnackPosition.BOTTOM,
    //     backgroundColor: Colors.green,
    //     margin: EdgeInsetsResponsive.only(bottom: 10, left: 10, right: 10));
  }
}
