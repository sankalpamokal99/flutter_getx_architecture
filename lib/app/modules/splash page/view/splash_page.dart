import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_getx_mvc/app/constants/assets.dart';
import 'package:flutter_getx_mvc/app/constants/colors.dart';
import 'package:flutter_getx_mvc/app/modules/splash%20page/controller/splash_controller.dart';
import 'package:flutter_getx_mvc/app/style/style_web.dart';
import 'package:get/get.dart';

class SplashPage extends GetView {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Hex476AD0,
      child: getUiForMobile(),
    ));
  }
}

Widget getUiForMobile() {
  return Column(
    children: [
      Container(
        height: Get.height,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  width: 87, height: 87, child: Image.asset(Assets.thankYou)),
              SizedBox(
                height: 25,
              ),
              Obx(() {
                return Style.getTitleText(
                    data: "Welcome Here, ${Get.find<SplashController>().name}");
              }),
              SizedBox(
                height: 100,
              ),
              Container(width: 227, height: 160, child: SizedBox()),
              SizedBox(
                height: 45,
              ),
              InkWell(
                child: Style.getText(data: "Click Here"),
                onTap: () {
                  Get.find<SplashController>().changeData();
                },
              )
            ],
          ),
        ),
      ),
    ],
  );
}
