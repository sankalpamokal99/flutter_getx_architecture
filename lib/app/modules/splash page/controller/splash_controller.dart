import 'package:get/get.dart';

class SplashController extends GetxController {
  RxString name = "Buddy".obs;
  void changeData() {
    print("inside changeData");
    name.value = "Sankalpa";
  }
}
