import 'package:flutter_getx_mvc/app/modules/home%20page/bindings/home_bindings.dart';
import 'package:flutter_getx_mvc/app/modules/home%20page/view/home_page.dart';
import 'package:flutter_getx_mvc/app/modules/splash%20page/bindings/splash_bindings.dart';
import 'package:flutter_getx_mvc/app/modules/splash%20page/view/splash_page.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.splashPage;

  static final routes = [
    GetPage(
        name: Routes.splashPage,
        page: () => SplashPage(),
        binding: SplashBindings()),
    GetPage(
        name: Routes.homePage, page: () => HomePage(), binding: HomeBindings()),
  ];
}
