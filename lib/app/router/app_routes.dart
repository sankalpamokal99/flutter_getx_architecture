part of 'app_pages.dart';

abstract class Routes {
  static const splashPage = '/splash';
  static const homePage = '/home';
}
