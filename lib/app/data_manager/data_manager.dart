class DataManager {
  static DataManager _instance = DataManager.internal();

  DataManager factory() {
    return _instance;
  }

  DataManager.internal();

  static DataManager get() {
    return _instance;
  }

  String idToken;
}
