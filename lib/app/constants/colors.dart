import 'package:flutter/material.dart';

const Color Hex476AD0 = Color(0xff476AD0);
const Color HexEEF2F2 = Color(0xffee5252);
const Color HexFFFFFF = Color(0xffFFFFFF);
const Color HexA2A2A2 = Color(0xffA2A2A2);
const Color HexEFF4F8 = Color(0xffEFF4F8);
const Color Hex333333 = Color(0xff333333);
const Color Hex808080 = Color(0xff808080);
const Color Hex395AB9 = Color(0xff395AB9);
const Color Hex404040 = Color(0xff404040);
const Color Hex3EAF76 = Color(0xff3EAF76);
const Color HexEA4435 = Color(0xffEA4435);
const Color HexDBDBDB = Color(0xffDBDBDB);
const Color Hex8298d2 = Color(0xff8298d2);
const Color HexA1B3E5 = Color(0xffA1B3E5);
const Color Hex65A2F4 = Color(0xff65A2F4);
const Color Hex4366CA = Color(0xff4366CA);
const Color Hex234195 = Color(0xff234195);
