import 'package:flutter/material.dart';

import 'colors.dart';

final ThemeData appThemeData = ThemeData(
  primaryColor: Hex476AD0,
  accentColor: Hex476AD0,
  splashColor: Hex476AD0,
  highlightColor: Hex476AD0,
  fontFamily: 'Georgia',
  textTheme: TextTheme(
    headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
  ),
);
