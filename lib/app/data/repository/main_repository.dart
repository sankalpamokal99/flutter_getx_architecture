import 'package:flutter_getx_mvc/app/data/provider/authentication_api_provider.dart';

class MainRepository {
  static MainRepository _instance = new MainRepository.internal();

  MainRepository.internal();

  factory MainRepository() => _instance;

  final AuthenticationProvider _authenticationProvider =
      AuthenticationProvider();

  Future<dynamic> authenticate(Map params) {
    return _authenticationProvider.authenticate(params);
  }
}
