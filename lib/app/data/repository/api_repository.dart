import 'package:flutter/material.dart';
import 'package:flutter_getx_mvc/app/data/provider/api.dart';

class ApiRepository {
  final ApiClient apiClient;

  ApiRepository({@required this.apiClient}) : assert(apiClient != null);
}
