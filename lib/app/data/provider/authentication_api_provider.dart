import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_getx_mvc/app/constants/app_constant.dart';
import 'api_provider.dart';

class AuthenticationProvider extends ApiProvider {
  Future<dynamic> authenticate(Map params) async {
    return authenticateWithParameters(params);
  }

  Future<dynamic> authenticateWithParameters(Map params) async {
    try {
      var url = "${AppConstant.ENDPOINT}";
      Response response = await dio.post(url, data: getDataAsBody(params));

      return response.data;
    } on DioError catch (e) {
      debugPrint(e.toString());

      if (e.response.statusCode == 400 ||
          e.response.statusCode == 409 ||
          e.response.statusCode == 401 ||
          e.response.statusCode == 404 ||
          e.response.statusCode == 500 ||
          e.response.statusCode == 417) {
        return e.response.data;
      }

      return null;
    } on Exception catch (e) {
      return null;
    }
  }
}
