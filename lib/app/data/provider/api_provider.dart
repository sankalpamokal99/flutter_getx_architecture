import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_getx_mvc/app/constants/app_constant.dart';

class ApiProvider {
  Dio dio;
  ApiProvider() {
    BaseOptions options = BaseOptions(
        receiveTimeout: 5000,
        connectTimeout: 5000,
        baseUrl: AppConstant.DEV_BASE_URL);
    dio = Dio(options);
    dio.interceptors..add(LogInterceptor(responseBody: true));
  }

  dynamic getDataAsBody(Map<String, dynamic> params) {
    return json.encode(params);
  }
}
